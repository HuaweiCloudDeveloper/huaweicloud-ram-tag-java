package com.huawei.demo;

// 用户身份认证
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.BatchCreateResourceShareTagsRequest;
import com.huaweicloud.sdk.ram.v1.model.BatchCreateResourceShareTagsResponse;
import com.huaweicloud.sdk.ram.v1.model.BatchDeleteResourceShareTagsRequest;
import com.huaweicloud.sdk.ram.v1.model.BatchDeleteResourceShareTagsResponse;
import com.huaweicloud.sdk.ram.v1.model.CreateResourceShareReqBody;
import com.huaweicloud.sdk.ram.v1.model.CreateResourceShareRequest;
import com.huaweicloud.sdk.ram.v1.model.CreateResourceShareResponse;
import com.huaweicloud.sdk.ram.v1.model.ListResourceShareTagsRequest;
import com.huaweicloud.sdk.ram.v1.model.ListResourceShareTagsResponse;
import com.huaweicloud.sdk.ram.v1.model.DeleteResourceShareRequest;
import com.huaweicloud.sdk.ram.v1.model.DeleteResourceShareResponse;
import com.huaweicloud.sdk.ram.v1.model.UntagResourceReqBody;
import com.huaweicloud.sdk.ram.v1.model.TagResourceReqBody;
import com.huaweicloud.sdk.ram.v1.model.Tag;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(RamTagDemo.class.getName());
    public static void main(String[] args) {
        // 创建认证
        String ak = "{your ak string}";
        String sk = "{your sk string}";

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamTagDemo demo = new RamTagDemo();

        // 1、创建资源共享
        CreateResourceShareResponse createResourceShareResponse = demo.createResourceShare(ramClient);
        // 2、为指定资源添加标签
        demo.batchCreateResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 3、查询已经使用的标签列表
        demo.listResourceShareTags(ramClient);
        // 4、删除资源共享实例的标签
        demo.batchDeleteResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 5、删除资源共享实例
        demo.deleteResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
    }

    public CreateResourceShareResponse createResourceShare(RamClient ramClient) {
        List<String> principals = new ArrayList<>();
        principals.add("your principals");  // 资源共享实例关联的一个或多个资源使用者的列表
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your urns");  // 资源共享实例关联的一个或多个共享资源URN的列表
        List<String> permissionIds = new ArrayList<>();
        permissionIds.add("your permission");  // 资源共享实例关联的RAM权限列表
        CreateResourceShareReqBody body = new CreateResourceShareReqBody();
        body.withResourceUrns(resourceUrns)
            .withPrincipals(principals)
            .withPermissionIds(permissionIds)
            .withDescription("your description")  // 资源共享实例的描述
            .withName("your share name");  // 资源共享实例的名称
        CreateResourceShareRequest request = new CreateResourceShareRequest().withBody(body);
        CreateResourceShareResponse response = null;
        try {
            response = ramClient.createResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return response;
    }

    public void batchCreateResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchCreateResourceShareTagsRequest request = new BatchCreateResourceShareTagsRequest();
        request.withResourceShareId(id);  // id:资源共享id
        TagResourceReqBody body = new TagResourceReqBody();
        List<Tag> Tags = new ArrayList<>();
        Tags.add(
            new Tag()
                .withKey("your tag key")
                .withValue("your tag value")
        );
        body.withTags(Tags);
        request.withBody(body);
        try {
            BatchCreateResourceShareTagsResponse response = ramClient.batchCreateResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void listResourceShareTags(RamClient ramClient) {
        ListResourceShareTagsRequest request = new ListResourceShareTagsRequest();
        try {
            ListResourceShareTagsResponse response = ramClient.listResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void batchDeleteResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchDeleteResourceShareTagsRequest request = new BatchDeleteResourceShareTagsRequest();
        request.withResourceShareId(id);  // id:资源共享id
        UntagResourceReqBody body = new UntagResourceReqBody();
        request.withBody(body);
        try {
            BatchDeleteResourceShareTagsResponse response = ramClient.batchDeleteResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        DeleteResourceShareRequest request = new DeleteResourceShareRequest()
            .withResourceShareId(id);  // id:资源共享实例的ID
        try {
            DeleteResourceShareResponse response = ramClient.deleteResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
