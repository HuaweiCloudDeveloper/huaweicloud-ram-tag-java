### 版本说明
本示例配套的SDK版本为：3.1.35及以上版本

### 示例简介
本示例展示如何使用资源访问管理（Resource Access Manager）相关SDK

### 功能介绍
资源访问管理标签管理相关功能

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


### SDK获取和安装
您可以通过Maven配置所依赖的资源访问管理服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：资源访问管理)

### 代码示例
以下代码展示如何使用资源访问管理（Resource Access Manager）相关SDK
``` java
package com.huawei.demo;
// 用户身份认证
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import com.huaweicloud.sdk.ram.v1.*;
import com.huaweicloud.sdk.ram.v1.model.*;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(RamTagDemo.class.getName());
    public static void main(String[] args) {
        // 创建认证
        String ak = "{your ak string}";
        String sk = "{your sk string}";

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamTagDemo demo = new RamTagDemo();

        // 1、创建资源共享
        CreateResourceShareResponse createResourceShareResponse = demo.createResourceShare(ramClient);
        // 2、为指定资源添加标签
        demo.batchCreateResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 3、查询已经使用的标签列表
        demo.listResourceShareTags(ramClient);
        // 4、删除资源共享实例的标签
        demo.batchDeleteResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 5、删除资源共享实例
        demo.deleteResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
    }

    public CreateResourceShareResponse createResourceShare(RamClient ramClient) {
        List<String> principals = new ArrayList<>();
        principals.add("your principals");  //资源共享实例关联的一个或多个资源使用者的列表
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your urns");  //资源共享实例关联的一个或多个共享资源URN的列表
        List<String> permissionIds = new ArrayList<>();
        permissionIds.add("your permission");  //资源共享实例关联的RAM权限列表
        CreateResourceShareReqBody body = new CreateResourceShareReqBody();
        body.withResourceUrns(resourceUrns)
            .withPrincipals(principals)
            .withPermissionIds(permissionIds)
            .withDescription("your description")  //资源共享实例的描述
            .withName("your share name");  //资源共享实例的名称
        CreateResourceShareRequest request = new CreateResourceShareRequest().withBody(body);
        CreateResourceShareResponse response = null;
        try {
            response = ramClient.createResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return response;
    }

    public void batchCreateResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchCreateResourceShareTagsRequest request = new BatchCreateResourceShareTagsRequest();
        request.withResourceShareId(id);  //id:资源共享id
        TagResourceReqBody body = new TagResourceReqBody();
        List<Tag> Tags = new ArrayList<>();
        Tags.add(
            new Tag()
                .withKey("your tag key")
                .withValue("your tag value")
        );
        body.withTags(Tags);
        request.withBody(body);
        try {
            BatchCreateResourceShareTagsResponse response = ramClient.batchCreateResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void listResourceShareTags(RamClient ramClient) {
        ListResourceShareTagsRequest request = new ListResourceShareTagsRequest();
        try {
            ListResourceShareTagsResponse response = ramClient.listResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void batchDeleteResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchDeleteResourceShareTagsRequest request = new BatchDeleteResourceShareTagsRequest();
        request.withResourceShareId(id);  //id:资源共享id
        UntagResourceReqBody body = new UntagResourceReqBody();
        request.withBody(body);
        try {
            BatchDeleteResourceShareTagsResponse response = ramClient.batchDeleteResourceShareTags(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        DeleteResourceShareRequest request = new DeleteResourceShareRequest()
            .withResourceShareId(id);  //id:资源共享实例的ID
        try {
            DeleteResourceShareResponse response = ramClient.deleteResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```
您可以在 [资源访问管理RAM服务文档](https://support.huaweicloud.com/productdesc-ram/ram_01_0001.html) 和[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RAM/doc?api=BatchCreateResourceShareTags) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2023/4/10 |1.0 | 文档首次发布


